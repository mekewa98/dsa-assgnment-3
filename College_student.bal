import ballerinax/kafka;
import ballerina/log;

kafka:ConsumerConfiguration consumrConfigs = {
    groupId: "group-id",
    topics: ["computingandinformatics.computingandinformatics.courseoutline"],
    pollingInterval: 1,
    autoCommit: true
};

final string DEFAULT_URL = "localhost:9092";

listener kafka:Listener kafkaListener = new (kafka:DEFAULT_URL, consumrConfigs);

service kafka:Service on kafkaListener {
    remote function onConsumrRecord(
        kafka:Caller caller,kafka:ConsumrRecord[] records) returns error? {

        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }
    }
}

function processKafkaRecord(kafka:ConsumrRecord kafkaRecord) returns error? {
    byte[] Content = kafkaRecord.value;
    string message = check string:fromBytes(Content);
    log:printInfo("Received Message: " + message);
}
